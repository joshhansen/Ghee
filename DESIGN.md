# Ghee Design Principles

1. Folders are tables
2. Files are records
3. Relative paths are primary keys
4. Extended attributes are table columns
5. Enforce schema only when present
6. The file contents and xattrs without the `user.ghee` prefix are user-controlled; only directory structure, filenames,
   and extended attributes in `user.ghee` are used by Ghee
7. Use of filesystem features should be preferred over implementing features directly in Ghee,
   e.g. locking, Btrfs subvolumes, snapshots, incremental backup
8. Indexes are just tables with a different primary key