# Ghee Roadmap

## 0.7
-[ ] `ghee branch`: branch management
-[ ] `ghee switch`: HEAD management
-[ ] `ghee diff`: see changes in detail, text files and binary
-[ ] Allow branch names to be given in place of commit hashes, esp. see `reset`
-[ ] Allow commit hash prefixes to be given so long as they are uniquely identifying
-[ ] Existence predicates, e.g. `-w name` matches any record for which `name` is set
-[ ] libghee (C library)
-[ ] Benchmarks
-[ ] Change hidden prefix from : to .
-[ ] `log`: go to pager on large output

## Future

-[ ] Separate "porcelain" from "plumbing" and test plumbing more thoroughly
-[ ] Unit test on ext4 and btrfs loop device filesystems?
-[ ] Test all applicable commands with empty invocation (no path provided, defaulting to current)
-[ ] `init`: allow comma-separated compound keys
-[ ] Virtual attribute `id` and attributes `id0` etc.
-[ ] `set`: don't require -s/--set flag; take attr=value pairs directly
-[ ] `get`: make output tabular as much as possible (get xattrs from all indices, put rest in 'other' column)
-[ ] `ls`: show default primary keys when not explicitly specified
-[ ] Issue warnings when xattrs inferred from path don't match explicit xattrs
* Enforce f64 vs String types, don't let them be comparable
* Generalize Btrfs functionality to other CoW filesystems, e.g. ZFS, Bcachefs, XFS
* Optimize more queries
* Multi-index query optimization
* Make sub-directories tables as well, on their portion of the key space?
