use anyhow::Result;

use std::path::PathBuf;

use ghee_lang::Xattr;

use super::cp_or_mv::{cp_or_mv, CopyOrMove};

/** Copy xattrs from `src` to `dest` */
pub fn cp(src: &PathBuf, dest: &PathBuf, fields: &Vec<Xattr>, verbose: bool) -> Result<()> {
    cp_or_mv(src, dest, fields, verbose, CopyOrMove::Copy)
}
