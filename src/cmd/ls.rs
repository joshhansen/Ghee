use anyhow::Result;

use std::path::Path;

use walkdir::WalkDir;

use crate::table_info;

/** List the contents of `paths` */
pub fn ls<P: AsRef<Path>>(paths: &Vec<P>, sort: bool) -> Result<()> {
    let mut paths: Vec<&Path> = paths.iter().map(|p| p.as_ref()).collect();

    if paths.is_empty() {
        paths.push(&Path::new("."));
    }

    for path in paths {
        let mut walker = WalkDir::new(path).min_depth(1).max_depth(1);

        if sort {
            walker = walker.sort_by_file_name();
        }

        for entry in walker {
            let entry = entry?;

            let child = entry.path();

            let name = child.components().last().unwrap();
            print!("{}\t", name.as_os_str().to_string_lossy());

            if child.is_dir() {
                let info = table_info(child);

                print!("table");

                if let Ok(info) = info {
                    if let Some(info) = info {
                        let key_s: Vec<String> = info.key().iter().map(|k| k.to_string()).collect();
                        print!("\tkey={}", key_s.join(", "));
                    }
                }
                println!();
            } else {
                assert!(child.is_file());

                println!("record");
            }
        }
    }

    Ok(())
}

#[cfg(test)]
mod test {
    use crate::test_support::Scenario;

    use super::ls;

    #[test]
    fn test_ls() {
        let s = Scenario::new("ls");

        // One directory
        ls(&vec![s.dir1.clone()], false).unwrap();

        // One file
        ls(&vec![s.dir2path2.clone()], true).unwrap();

        // Mix
        ls(
            &vec![s.dir1.clone(), s.dir2path2.clone(), s.dir2.clone()],
            false,
        )
        .unwrap();

        // Mix, sorted
        ls(
            &vec![s.dir1.clone(), s.dir2path2.clone(), s.dir2.clone()],
            true,
        )
        .unwrap();
    }
}
