use std::env::current_dir;

use anyhow::Result;
use thiserror::Error;

use crate::{containing_table_info, XATTR_HEAD};

use super::{restore::reset_path, NewFileHandling};

#[derive(Error, Debug)]
pub enum ResetErr {
    #[error("Current directory not contained by a table")]
    NotInTable,
}

/**
* Return the current table to the state it had in the commit with the given uuid `commit_uuid`
*
* ## Errors
*
* If the current working directory is not contained by a table, an error will be returned.
* An error also results if the commit hash does not match any commit.
*/
pub fn reset(commit_uuid: &str, verbose: bool, new_file_handling: NewFileHandling) -> Result<()> {
    let cur = current_dir()?;
    let info = containing_table_info(&cur)?.ok_or(ResetErr::NotInTable)?;

    let table_path = info.path_abs();

    reset_path(table_path, commit_uuid, true, verbose, new_file_handling)?;

    xattr::set(table_path, XATTR_HEAD.to_osstring(), commit_uuid.as_bytes())?;

    Ok(())
}
