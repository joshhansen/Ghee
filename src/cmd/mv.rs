use anyhow::Result;
use ghee_lang::Xattr;

use std::path::PathBuf;

use super::cp_or_mv::{cp_or_mv, CopyOrMove};

/** Move xattrs from `src` to `dest` */
pub fn mv(src: &PathBuf, dest: &PathBuf, fields: &Vec<Xattr>, verbose: bool) -> Result<()> {
    cp_or_mv(src, dest, fields, verbose, CopyOrMove::Move)
}
