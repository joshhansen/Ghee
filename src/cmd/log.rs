use std::path::PathBuf;

use anyhow::Result;
use colored::Colorize;
use ghee_lang::Value;

use crate::{
    containing_table_info, paths::table_snapshot_path, xattr_value, XATTR_COMMIT_MESSAGE,
    XATTR_HEAD,
};

pub fn log(dir: &PathBuf) -> Result<()> {
    if let Some(info) = containing_table_info(dir)? {
        let table_path = info.path_abs();

        if let Ok(mut most_recent_snapshot) = xattr_value(&table_path, &XATTR_HEAD) {
            let mut snapshot_path = Some(table_snapshot_path(table_path, &most_recent_snapshot));

            while let Some(sp) = snapshot_path.take() {
                let commit = format!("commit {}", most_recent_snapshot).yellow();
                println!("{}", commit);
                let message = xattr_value(&sp, &XATTR_COMMIT_MESSAGE)
                    .unwrap_or(Value::String("--No message--".to_string()));
                println!("\n\t{}\n", message);

                if let Ok(less_recent_snapshot) = xattr_value(&sp, &XATTR_HEAD) {
                    snapshot_path = Some(table_snapshot_path(table_path, &less_recent_snapshot));
                    most_recent_snapshot = less_recent_snapshot;
                } else {
                    snapshot_path = None;
                }
            }
        } else {
            println!("No commit log exists; use `commit` to commit changes");
        }
    } else {
        println!("No table found; no log exists");
    }

    Ok(())
}
