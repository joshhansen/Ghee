use std::{collections::BTreeMap, path::PathBuf};

use serde::{Deserialize, Serialize};

use crate::parser::key::Key;

#[derive(Serialize, Deserialize)]
pub struct IndexInfo {
    /// The indexed key and location of related indices
    pub related_indices: BTreeMap<Key, PathBuf>,
}

impl Default for IndexInfo {
    fn default() -> Self {
        IndexInfo {
            related_indices: BTreeMap::new(),
        }
    }
}
