use nom::bytes::complete::is_not;
use nom::combinator::{all_consuming, map};
use nom::multi::many0;
use nom::number::complete::double;

use nom::bytes::complete::take_while1;

use nom::character::{is_alphanumeric, is_space};

use nom::bytes::complete::tag;

use nom::sequence::delimited;

use nom::branch::alt;

use nom::IResult;
use serde::Serialize;
use thiserror::Error;

use std;

//FIXME Enforce types statically; don't let String be compared with f64
#[derive(Clone, Debug, PartialEq, PartialOrd, Serialize)]
pub enum Value {
    Number(f64),
    String(String),
    Bytes(Vec<u8>),
}

#[derive(Error, Debug)]
pub enum ValueErr {
    #[error("JSON value was not supported: {0}")]
    UnsupportedJsonValue(serde_json::Value),
}

fn is_not_space(chr: u8) -> bool {
    !is_space(chr)
}

fn is_alphanumeric_or_dash(chr: u8) -> bool {
    is_alphanumeric(chr) || chr == '-' as u8
}

impl Value {
    pub fn as_bytes(&self) -> Vec<u8> {
        match self {
            Self::Number(x) => format!("{}", x).into_bytes(),
            Self::String(s) => {
                let mut bytes = s.clone().into_bytes();

                let alphanum = bytes.iter().copied().all(is_not_space);

                if !alphanum {
                    bytes.insert(0, b'"');
                    bytes.push(b'"');
                }

                bytes
            }
            Self::Bytes(b) => b.clone(),
        }
    }
}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            Self::Number(x) => write!(f, "{}", x),
            Self::String(s) => write!(f, "{}", s),
            Self::Bytes(b) => write!(f, "{}", String::from_utf8_lossy(b)),
        }
    }
}

impl From<f64> for Value {
    fn from(x: f64) -> Self {
        Self::Number(x)
    }
}

impl From<String> for Value {
    fn from(s: String) -> Self {
        Self::String(s)
    }
}

impl From<&str> for Value {
    fn from(s: &str) -> Self {
        Self::String(s.to_string())
    }
}

impl From<&[u8]> for Value {
    fn from(b: &[u8]) -> Self {
        parse_value(b).unwrap().1
    }
}

impl TryFrom<serde_json::Value> for Value {
    type Error = ValueErr;
    fn try_from(v: serde_json::Value) -> Result<Self, Self::Error> {
        match v {
            serde_json::Value::Number(x) => {
                Ok(Value::Number(x.as_f64().unwrap_or_else(|| {
                    panic!("Could not convert json number {} to ghee number", x)
                })))
            }
            serde_json::Value::String(s) => Ok(Value::String(s)),
            x => Err(ValueErr::UnsupportedJsonValue(x)),
        }
    }
}

pub fn string(i: &[u8]) -> IResult<&[u8], String> {
    alt((
        delimited(tag("'"), is_not("'"), tag("'")),
        delimited(tag("\""), is_not("\""), tag("\"")),
        take_while1(is_alphanumeric_or_dash),
    ))(i)
    .map(|(i, b)| (i, String::from_utf8_lossy(b).to_string()))
}

pub fn parse_value_not_all_consuming(i: &[u8]) -> IResult<&[u8], Value> {
    alt((
        map(double, Value::Number),
        map(string, Value::String),
        map(many0(nom::number::complete::u8), Value::Bytes),
    ))(i)
}

pub fn parse_value(i: &[u8]) -> IResult<&[u8], Value> {
    alt((
        map(all_consuming(double), Value::Number),
        map(all_consuming(string), Value::String),
        map(
            all_consuming(many0(nom::number::complete::u8)),
            Value::Bytes,
        ),
    ))(i)
}

#[cfg(test)]
mod test {
    use crate::parser::value::{parse_value, Value};

    #[test]
    fn test_parse_value() {
        assert_eq!(
            parse_value(b"5.5"),
            Ok((b"".as_slice(), Value::Number(5.5f64)))
        );
        assert_eq!(
            parse_value(b"Abcdefg"),
            Ok((b"".as_slice(), Value::String(String::from("Abcdefg"))))
        );
        assert_eq!(
            parse_value(b"\x00"),
            Ok((b"".as_slice(), Value::Bytes(b"\x00".to_vec())))
        );
        assert_eq!(
            parse_value(b"59b49924-d34f-3e43-b645-d3d2b1c4f51"),
            Ok((
                b"".as_slice(),
                Value::String(String::from("59b49924-d34f-3e43-b645-d3d2b1c4f51"))
            ))
        );
    }
}
