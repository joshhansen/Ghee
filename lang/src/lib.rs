use std::collections::BTreeMap;

use nom::{
    bytes::complete::take_while, character::complete::char, character::is_space,
    multi::separated_list0, sequence::delimited, IResult,
};

pub type Record = BTreeMap<Xattr, Value>;

pub fn space(i: &[u8]) -> IResult<&[u8], &[u8]> {
    take_while(move |x| is_space(x))(i)
}

pub fn array(i: &[u8]) -> IResult<&[u8], Vec<Value>> {
    delimited(
        char('['),
        separated_list0(
            char(','),
            delimited(space, parse_value_not_all_consuming, space),
        ),
        char(']'),
    )(i)
}

#[test]
fn test_array() {
    assert_eq!(
        array(b"[1,2,3,4]".as_slice()),
        Ok((
            b"".as_slice(),
            vec![1f64, 2f64, 3f64, 4f64]
                .into_iter()
                .map(Value::from)
                .collect()
        ))
    );
    assert_eq!(
        array(b"[ 1 ,2 , 3, A, \"B\" ,\"C\",Defg]"),
        Ok((
            b"".as_slice(),
            vec![
                Value::from(1f64),
                Value::from(2f64),
                Value::from(3f64),
                Value::from("A"),
                Value::from("B"),
                Value::from("C"),
                Value::from("Defg")
            ]
        ))
    );
}

mod assignment;
mod key;
mod predicate;
mod relation;
mod value;
mod xattr;

pub use assignment::*;
pub use key::*;
pub use predicate::*;
pub use relation::*;
pub use value::*;
pub use xattr::*;
