use nom::{
    branch::alt,
    bytes::complete::tag,
    combinator::map,
    sequence::{pair, preceded},
    IResult,
};

use super::{
    array, space,
    value::{parse_value, Value},
};

#[derive(Clone, Debug, PartialEq)]
pub enum Relation {
    Eq(Value),
    Gt(Value),
    Gte(Value),
    Lt(Value),
    Lte(Value),
    In(Vec<Value>),
}
#[allow(dead_code)]
impl Relation {
    pub fn eq<V: Into<Value>>(x: V) -> Self {
        Self::Eq(x.into())
    }
    pub fn gt<V: Into<Value>>(x: V) -> Self {
        Self::Gt(x.into())
    }
    pub fn gte<V: Into<Value>>(x: V) -> Self {
        Self::Gte(x.into())
    }
    pub fn lt<V: Into<Value>>(x: V) -> Self {
        Self::Lt(x.into())
    }
    pub fn lte<V: Into<Value>>(x: V) -> Self {
        Self::Lte(x.into())
    }
    pub fn in_<V: Into<Value>>(x: Vec<V>) -> Self {
        Self::In(x.into_iter().map(|x| x.into()).collect())
    }

    pub fn satisfied_by<V: Into<Value>>(&self, x: V) -> bool {
        let x: Value = x.into();
        match self {
            Self::Eq(v) => *v == x,
            Self::Gt(v) => x > *v,
            Self::Gte(v) => x >= *v,
            Self::Lt(v) => x < *v,
            Self::Lte(v) => x <= *v,
            Self::In(v) => v.contains(&x),
        }
    }
}

#[test]
fn test_satisfied_by() {
    {
        let r = parse_relation(b"< -2").unwrap().1;
        assert!(r.satisfied_by(-3f64));
        assert!(!r.satisfied_by(-2f64));
    }
    {
        let r = parse_relation(b"=ABC").unwrap().1;
        assert!(r.satisfied_by("ABC"));
        assert!(!r.satisfied_by("ABCD"));
    }
    {
        let r = parse_relation(b"in [1, 2, 3]").unwrap().1;
        assert!(r.satisfied_by(1f64));
        assert!(r.satisfied_by(2f64));
        assert!(r.satisfied_by(3f64));
        assert!(!r.satisfied_by(0f64));
        assert!(!r.satisfied_by(1.1f64));
    }
}
/// E.g. `> 5`
pub fn parse_relation(i: &[u8]) -> IResult<&[u8], Relation> {
    alt((
        preceded(pair(tag(">="), space), map(parse_value, Relation::Gte)),
        preceded(pair(tag("<="), space), map(parse_value, Relation::Lte)),
        preceded(pair(tag("in"), space), map(array, Relation::in_)),
        preceded(pair(tag(">"), space), map(parse_value, Relation::Gt)),
        preceded(pair(tag("<"), space), map(parse_value, Relation::Lt)),
        preceded(pair(tag("="), space), map(parse_value, Relation::Eq)),
    ))(i)
}

#[test]
fn test_parse_relation() {
    assert!(parse_relation(b"a + b").is_err());
    assert_eq!(
        parse_relation(b">=-2.4343").unwrap().1,
        Relation::gte(-2.4343f64)
    );
    assert_eq!(parse_relation(b"<= -2").unwrap().1, Relation::lte(-2f64));
    assert_eq!(
        parse_relation(b"in [1,2,3]"),
        Ok((b"".as_slice(), Relation::in_(vec![1f64, 2f64, 3f64])))
    );
    assert_eq!(
        parse_relation(b"> 4"),
        Ok((b"".as_slice(), Relation::gt(4f64)))
    );
    assert_eq!(
        parse_relation(b"< 4"),
        Ok((b"".as_slice(), Relation::lt(4f64)))
    );
}
