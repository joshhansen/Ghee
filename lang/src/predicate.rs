use std::ffi::OsStr;

use clap::{builder::TypedValueParser, Arg, Command};
use nom::{
    sequence::{pair, terminated},
    IResult,
};

use crate::Record;

use super::{
    relation::{parse_relation, Relation},
    space,
    xattr::{parse_xattr, Xattr},
};

#[derive(Clone, Debug, PartialEq)]
pub struct Predicate {
    pub xattr: Xattr,
    pub relation: Relation,
}

impl Predicate {
    pub fn new(xattr: Xattr, relation: Relation) -> Self {
        Self { xattr, relation }
    }
    pub fn satisfied(&self, values: &Record) -> bool {
        values
            .get(&self.xattr)
            .map_or(false, |v| self.relation.satisfied_by(v.clone()))
    }
}

// fn parse_predicate<'a, E: ParseError<&'a str>>(i: &'a str) -> IResult<&'a str, Predicate, E> {
//     pair(parse_xattr, parse_relation)(i)
//         .map(|(i, (xattr, relation))| (i, Predicate { xattr, relation }))
// }

pub fn parse_predicate(i: &[u8]) -> IResult<&[u8], Predicate> {
    pair(terminated(parse_xattr, space), parse_relation)(i)
        .map(|(i, (xattr, relation))| (i, Predicate { xattr, relation }))
}

#[derive(Clone)]
pub struct PredicateParser;
impl TypedValueParser for PredicateParser {
    type Value = Predicate;

    fn parse_ref(
        &self,
        _cmd: &Command,
        _arg: Option<&Arg>,
        value: &OsStr,
    ) -> Result<Self::Value, clap::error::Error<clap::error::RichFormatter>> {
        parse_predicate(value.to_string_lossy().as_bytes())
            .map(|(_remainder, predicate)| predicate)
            .map_err(|e| {
                clap::error::Error::raw(
                    clap::error::ErrorKind::InvalidValue,
                    format!("Malformed WHERE clause: {}\n", e),
                )
            })
    }
}

#[cfg(test)]
mod test {
    use crate::parser::{
        relation::Relation,
        xattr::{Namespace, Xattr},
    };

    use super::{parse_predicate, Predicate};
    #[test]
    fn test_parse_predicate() {
        assert!(parse_predicate(b"").is_err());
        assert!(parse_predicate(b"a + b").is_err());
        assert!(parse_predicate(b"fakenamespace.lmnop").is_err());
        assert_eq!(
            parse_predicate(b"amplitude>=-2.4343").unwrap().1,
            Predicate {
                xattr: Xattr::new(Namespace::User, "amplitude"),
                relation: Relation::gte(-2.4343f64)
            }
        );
        assert_eq!(
            parse_predicate(b"system.security <= -2").unwrap().1,
            Predicate {
                xattr: Xattr::new(Namespace::System, "security"),
                relation: Relation::lte(-2f64)
            }
        );
        assert_eq!(
            parse_predicate(b"security.lmnop in [1,2,3]"),
            Ok((
                b"".as_slice(),
                Predicate {
                    xattr: Xattr::new(Namespace::Security, "lmnop"),
                    relation: Relation::in_(vec![1f64, 2f64, 3f64])
                }
            ))
        );
        assert_eq!(
            parse_predicate(b"trusted.trustfulness > 4"),
            Ok((
                b"".as_slice(),
                Predicate {
                    xattr: Xattr::new(Namespace::Trusted, "trustfulness"),
                    relation: Relation::gt(4f64)
                }
            ))
        );
        assert_eq!(
            parse_predicate(b"user.grumpiness < 4"),
            Ok((
                b"".as_slice(),
                Predicate {
                    xattr: Xattr::new(Namespace::User, "grumpiness"),
                    relation: Relation::lt(4f64)
                }
            ))
        );
    }
}
