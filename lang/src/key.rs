use std::{
    ops::{Index, IndexMut},
    path::PathBuf,
    slice::Iter,
};

use anyhow::Result;
use nom::{character::complete::char, combinator::map, multi::separated_list1, IResult};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use thiserror::Error;

use crate::Record;

use super::{
    value::Value,
    xattr::{parse_xattr, StringVisitor, Xattr},
};

#[derive(Error, Debug)]
pub enum KeyErr {
    #[error("The subkey {subkey} had no value on path {path}")]
    SubkeyValueNotFoundOnPath { subkey: Xattr, path: PathBuf },

    #[error("The subkey {subkey} had no value in record {record:?}")]
    SubkeyValueNotFoundInRecord { subkey: Xattr, record: Record },

    #[error("Record must be JSON object")]
    RecordNotObject,
}

#[derive(Clone, Debug, PartialEq, Eq, Ord, PartialOrd)]
pub struct Key {
    pub subkeys: Vec<Xattr>,
}

impl Key {
    pub fn new(subkeys: Vec<Xattr>) -> Self {
        Self { subkeys }
    }

    pub fn from_string<S: ToString>(s: S) -> Self {
        Self::from(s.to_string().split(","))
    }

    pub fn is_empty(&self) -> bool {
        self.subkeys.is_empty()
    }

    pub fn len(&self) -> usize {
        self.subkeys.len()
    }

    pub fn to_string(&self) -> String {
        if self.subkeys.is_empty() {
            return String::from("");
        }
        // subkeys.len() - 1 is added to account for the commas
        let len = self
            .subkeys
            .iter()
            .map(|k| k.len())
            .reduce(|acc, e| acc + e)
            .unwrap_or_default()
            + self.subkeys.len()
            - 1;

        let mut s = String::with_capacity(len);

        for (i, k) in self.subkeys.iter().enumerate() {
            if i > 0 {
                s.push(',');
            }
            s.push_str(k.to_string().as_str());
        }

        s
    }

    pub fn to_minimal_string(&self) -> String {
        if self.subkeys.is_empty() {
            return String::from("");
        }
        // subkeys.len() - 1 is added to account for the commas
        let len = self
            .subkeys
            .iter()
            .map(|k| k.minimal_len())
            .reduce(|acc, e| acc + e)
            .unwrap_or_default()
            + self.subkeys.len()
            - 1;

        let mut s = String::with_capacity(len);

        for (i, k) in self.subkeys.iter().enumerate() {
            if i > 0 {
                s.push(',');
            }
            s.push_str(k.to_minimal_string().as_str());
        }

        s
    }

    pub fn iter(&self) -> Iter<Xattr> {
        self.subkeys.iter()
    }

    pub fn value_for_record(&self, record: &Record) -> Result<Vec<Value>> {
        let mut values: Vec<Value> = Vec::with_capacity(self.subkeys.len());

        for subkey in self.subkeys.iter() {
            let value = record
                .get(&subkey)
                .ok_or_else(|| KeyErr::SubkeyValueNotFoundInRecord {
                    subkey: subkey.clone(),
                    record: record.clone(),
                })?
                .clone();

            values.push(value);
        }

        Ok(values)
    }

    pub fn path_for_record(&self, mut base_path: PathBuf, record: &Record) -> Result<PathBuf> {
        let parts = self.value_for_record(record)?;

        for part in parts {
            base_path.push(part.to_string());
        }

        Ok(base_path)
    }
}

impl<S: ToString, I: IntoIterator<Item = S>> From<I> for Key {
    fn from(keys: I) -> Self {
        let xattrs: Vec<Xattr> = keys
            .into_iter()
            .map(|k| k.to_string().into_bytes())
            .map(|b| {
                let (remainder, xattr) = parse_xattr(b.as_slice()).unwrap();
                debug_assert_eq!(remainder.len(), 0);
                xattr
            })
            .collect();

        Self::new(xattrs)
    }
}

impl Index<usize> for Key {
    type Output = Xattr;
    fn index(&self, index: usize) -> &Self::Output {
        &self.subkeys[index]
    }
}

impl IndexMut<usize> for Key {
    fn index_mut(&mut self, index: usize) -> &mut Self::Output {
        &mut self.subkeys[index]
    }
}

impl Serialize for Key {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.to_minimal_string().as_str())
    }
}

impl<'a> Deserialize<'a> for Key {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'a>,
    {
        let s = deserializer.deserialize_string(StringVisitor).unwrap();
        Ok(Self::from_string(s))
    }
}

/// A primary key, possibly compound
pub fn parse_key(i: &[u8]) -> IResult<&[u8], Key> {
    map(separated_list1(char(' '), parse_xattr), Key::new)(i)
}

#[cfg(test)]
mod test {
    use crate::{xattr::parse_xattr, Key};

    #[test]
    fn test_from_string_iter() {
        assert_eq!(
            Key::from(vec!["a", "b", "system.c"]),
            Key::new(vec![
                parse_xattr(b"a").unwrap().1,
                parse_xattr(b"b").unwrap().1,
                parse_xattr(b"system.c").unwrap().1
            ])
        );
    }
}
