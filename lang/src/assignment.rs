use std::ffi::OsStr;

use clap::{builder::TypedValueParser, Arg, Command};
use nom::{character::complete::char, combinator::map, sequence::separated_pair, IResult};

use super::{
    value::{parse_value, Value},
    xattr::{parse_xattr, Xattr},
};

#[derive(Clone)]
pub struct Assignment {
    pub xattr: Xattr,
    pub value: Value,
}
impl Assignment {
    pub fn new(xattr: Xattr, value: Value) -> Self {
        Self { xattr, value }
    }
}

impl std::fmt::Display for Assignment {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{}={}", self.xattr, self.value)
    }
}

pub fn parse_assignment(i: &[u8]) -> IResult<&[u8], Assignment> {
    map(
        separated_pair(parse_xattr, char('='), parse_value),
        |(xattr, value)| Assignment { xattr, value },
    )(i)
}

#[derive(Clone)]
pub struct AssignmentParser;
impl TypedValueParser for AssignmentParser {
    type Value = Assignment;

    fn parse_ref(
        &self,
        _cmd: &Command,
        _arg: Option<&Arg>,
        value: &OsStr,
    ) -> Result<Self::Value, clap::error::Error<clap::error::RichFormatter>> {
        parse_assignment(value.to_string_lossy().as_bytes())
            .map(|(_remainder, predicate)| predicate)
            .map_err(|e| {
                clap::error::Error::raw(
                    clap::error::ErrorKind::InvalidValue,
                    format!("Malformed WHERE clause: {}\n", e),
                )
            })
    }
}
