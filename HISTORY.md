# Ghee History

Ghee's development began as an adjunct to the the [Audiotater](https://git.disroot.org/joshhansen/audiotater) audio annotation tool,
but soon developed a life of its own.

## 0.1.0 - 2 Aug 2023 (as "Hatter")

Initial release including commands to manipulate individual records: `cp`, `mv`, `rm`, `get`, `set`.

### 0.2.0 (as "Mattress")

First command that operates on records as a group ("table"): `idx`.

Also generates shell completions for Bash, Fish, PowerShell, Elvish, and anything else that `clap_complete` supports.

Rename from Hatter to Mattress.

### 0.2.1 (as "Mattress")

Implement setting of user.mattress.keyname

### 0.3.0 - 10 Aug 2023

* Make `get` recursive by default
* SQL WHERE-style select clauses using `-w --where`
* Rename from Mattress to Ghee. Third time's the charm, right?

### 0.4.0 - 30 Aug 2023

Introduces the REPL and subcommands `init`, `ins`, `del`, and `ls`.

Both `get` and `del` now use the best available index to minimize traversals.

-[x] REPL
-[x] `ins` for adding to table while maintaining related indexes
-[x] `ins` from JSON
-[x] `del` for removing from table while maintaining related indexes
-[x] `ls` to show Ghee's view of the world
-[x] `init` to initialize a directory as a full-fledged Ghee table (with specified key names)
-[x] Sensible default destination for `idx`
-[x] Make `get` stop listing nested indices
-[x] Make `get` accelerate traversal using the best available index (a la `del`)
-[x] Make `get` return paths from original index rather than the one used to accelerate
-[x] Fix output order, making commands reproducible

### 0.5.0 - 19 Sep 2023

Maintains soundness of indices when `rm`ing, `set`ing, `cp`ing, and `mv`ing.

Makes `rm` and `set` recursive, with optional opt-out.

Unit tests all commands.

Introduces `create`; like `init` but creates the directory for you.

Adds the ability to insert from JSON on `init`.

-[x] `init`: allow `ins`-like insertion from JSON
-[x] `create` to create directory and then `init`
-[x] Test predicates using default key names, e.g. `key0<10`
-[x] `rm`: recursive by default
-[x] `rm`: when removing indexed xattrs, update the relevant indices
-[x] `set`: recursive by default
-[x] `set`: when setting indexed xattrs, update the relevant indices
-[x] Test `cp_or_mv`
-[x] Test `ls`

## 0.5.1

Fixes a bug when indices were chained together but not reflected in all related indices' table info.

-[x] `idx`: ensure that indices beyond the second result in all related indices being declared as related

## 0.6.0 - 2 Oct 2023

Introduces tools for snapshotting and rolling back the state of Ghee tables, built using Btrfs subvolume snapshots.

The `commit` command makes a snapshot of the current state; `log` lists past changes; `restore` returns files to their
`HEAD` state; and `reset` returns all files to their state in a specified commit.

As in Git, a reference to a `HEAD` commit is maintained, but Ghee stores it in the extended attribute `user.ghee.v0.HEAD`.

(Versioning of the `user.ghee` xattr namespace is also accomplished in this release.)

A `touch` command is introduced to allow easy creation of files, whose xattrs will be inferred from their position within the
table / index directory structure.

Snapshot testing has been greatly strengthened, testing against ext4 and btrfs filesystems as well as relative and absolute paths.

-[x] ~~`init`~~: `create`: create as a Btrfs subvolume when possible
-[x] `ghee commit ./people -m "message!"`: commit the `./people` table in its current form with message "message!"
-[x] `ghee log ./people`: show commit messages for all commits in the `./people` table.
-[x] `commit`: inherit snapshot dir ownership from parent
-[x] `commit`: unit test; implemented, but disabled as it requires superuser
-[x] `ghee status ./people`: show how the ./people table has changed since last commit; using CoW snapshots
-[x] `ghee reset ./people gf037d2c98`: replace the current contents of the `./people` table to its state in commit gf037d2c98
-[x] `ghee restore ./people`: replace all changed files in `./people` with their versions in the most recent commit
-[x] Test `get` on non-table? Whatever that bug is...
-[x] Ensure all commands accept relative paths
-[-] Maybe store table index paths as relative paths? (Then we can operate on snapshots directly.)
     Rejected in favor of storing absolute paths, which we then relativize
-[x] Cover absolute paths in example.sh
-[x] Version user.ghee namespace
-[x] Rename most-recent-snapshot -> HEAD
-[x] Update README.md for 0.6

## 0.6.1 - 2 Oct 2023

Points the `btrfsutil` dependency at a published crate version rather than Git repository.