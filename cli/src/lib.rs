use std::path::PathBuf;

use clap::{builder::ValueParser, Parser, Subcommand};
use thiserror::Error;

use ghee_lang::{Assignment, AssignmentParser, Predicate, PredicateParser, Value, Xattr};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
pub struct Cli {
    /// Subcommand to run. If none is provided, a REPL will be launched
    #[command(subcommand)]
    pub command: Option<Commands>,
}

#[derive(Error, Debug)]
pub enum TableOpenErr {
    #[error("Table path could not be opened because it doesn't exist")]
    NoSuchPath,
}

#[derive(Subcommand)]
pub enum Commands {
    /// Copy xattr values from one path to another.
    Cp {
        #[arg(help = "Path to copy xattrs from")]
        src: PathBuf,
        #[arg(help = "Path to copy xattrs to")]
        dest: PathBuf,
        #[arg(name = "field", short, long, help = "xattrs to copy")]
        fields: Vec<Xattr>,
        #[arg(short, long)]
        verbose: bool,
    },
    /// Move xattr values from one path to another.
    Mv {
        #[arg(help = "Path to move xattrs from")]
        src: PathBuf,
        #[arg(help = "Path to move xattrs to")]
        dest: PathBuf,
        #[arg(name = "field", short, long, help = "xattrs to move")]
        fields: Vec<Xattr>,
        #[arg(short, long)]
        verbose: bool,
    },
    /// Remove xattr values.
    Rm {
        #[arg(name = "path", help = "Paths to remove xattrs from", required = true)]
        paths: Vec<PathBuf>,
        #[arg(name = "field", short, long, help = "xattrs to remove")]
        fields: Vec<Xattr>,
        #[arg(long, help = "Process paths nonrecursively; defaults to false")]
        flat: bool,
        #[arg(
            long,
            help = "Don't complain when an xattr doesn't exist; defaults to false"
        )]
        force: bool,
        #[arg(short, long)]
        verbose: bool,
    },

    /// Get and print xattr values for one or more paths.
    Get {
        #[arg(name = "path", help = "Paths to get xattrs from", required = true)]
        paths: Vec<PathBuf>,
        #[arg(name = "field", short, long, help = "xattrs to get")]
        fields: Vec<Xattr>,
        #[arg(short, long, help = "Output JSON; lossily decodes as UTF-8")]
        json: bool,
        #[arg(name = "where", short, long, help = "WHERE clause to filter results by", value_parser = ValueParser::new(PredicateParser{}))]
        where_: Vec<Predicate>,
        #[arg(long, help = "Process paths nonrecursively; defaults to false")]
        flat: bool,
        #[arg(short, long, help = "Include user.ghee prefix in output")]
        all: bool,
        #[arg(
            short,
            long,
            help = "Sort directory contents as paths are walked; disable for potential speedup on large listings",
            default_value_t = true
        )]
        sort: bool,
        #[arg(short, long, help = "Return records even when apparently empty")]
        visit_empty: bool,
    },

    /// Set xattr values
    Set {
        #[arg(name = "paths", help = "Paths to set xattrs on", required = true)]
        paths: Vec<PathBuf>,
        #[arg(name = "set", short, long, help = "k=v pairs to set", value_parser = ValueParser::new(AssignmentParser{}))]
        field_assignments: Vec<Assignment>,
        #[arg(long, help = "Process paths nonrecursively; defaults to false")]
        flat: bool,
        #[arg(short, long)]
        verbose: bool,
    },

    /// Insert records into a table, updating related indices
    Ins {
        #[arg(help = "Path of the table to insert into")]
        table_path: PathBuf,
        #[arg(
            help = "Path of the records, JSON, one record per line; if omitted, the same format is taken from stdin"
        )]
        records_path: Option<PathBuf>,
        #[arg(short, long)]
        verbose: bool,
    },

    /// Remove record from a table, updating related indices
    Del {
        #[arg(help = "Path of the table to delete from")]
        table_path: PathBuf,
        #[arg(name = "where", short, long, help = "WHERE clauses specifying what to delete", value_parser = ValueParser::new(PredicateParser{}))]
        where_: Vec<Predicate>,
        #[arg(help = "Primary key values, subkeys space separated")]
        key: Vec<Value>,
        #[arg(short, long)]
        verbose: bool,
    },

    /// Index tables
    Idx {
        #[arg(help = "Path to recursively index")]
        src: PathBuf,
        #[arg(help = "Path to output the new index to")]
        dest: Option<PathBuf>,
        #[arg(name = "key", short, long, help = "xattrs to index by")]
        keys: Vec<Xattr>,
        #[arg(short, long)]
        verbose: bool,
    },

    /// List directory contents as seen by Ghee
    Ls {
        #[arg(help = "Paths to list the contents of; current directory by default")]
        paths: Vec<PathBuf>,

        #[arg(
            short,
            long,
            help = "Sort directory contents; disable for potential speedup on large listings",
            default_value_t = true
        )]
        sort: bool,
    },

    /// Initialize a directory as a Ghee table with specified primary key, then optionally insert records
    Init {
        #[arg(help = "Directory to initialize as a Ghee table")]
        dir: PathBuf,

        #[arg(
            name = "key",
            short,
            long,
            help = "xattrs the table will be indexed by"
        )]
        keys: Vec<Xattr>,

        #[arg(
            help = "Path of the records to insert, JSON, one record per line; if omitted, the same format is taken from stdin"
        )]
        records_path: Option<PathBuf>,

        #[arg(short, long)]
        verbose: bool,
    },

    /// Like `init`, but creates the directory first, or errors if one exists already
    Create {
        #[arg(help = "Directory to create and initialize as a Ghee table")]
        dir: PathBuf,

        #[arg(
            name = "key",
            short,
            long,
            help = "xattrs the table will be indexed by"
        )]
        keys: Vec<Xattr>,

        #[arg(
            help = "Path of the records to insert, JSON, one record per line; if omitted, the same format is taken from stdin"
        )]
        records_path: Option<PathBuf>,

        #[arg(short, long)]
        verbose: bool,
    },

    /// Commit a table, storing its contents as a BTRFS snapshot
    Commit {
        #[arg(help = "Directory of the table to commit, or the current directory by default")]
        dir: Option<PathBuf>,

        #[arg(short, long, help = "A message describing the changes being committed")]
        message: Option<String>,

        #[arg(short, long)]
        verbose: bool,
    },

    /// Print the commit log
    Log {
        #[arg(
            help = "Directory of the table whose log to print, or the current directory by default"
        )]
        dir: Option<PathBuf>,
    },

    /// Create a file, inferring xattrs from path if part of a table
    Touch {
        #[arg(help = "Path at which to create the file")]
        path: PathBuf,
        #[arg(short, long, help = "Create parent directories")]
        parents: bool,
    },

    /// Print the status of files in the table, relative to the most recent commit (if any)
    Status {
        #[arg(
            help = "Path of a file in a table; the whole table's status will be listed; defaults to the current directory"
        )]
        path: Option<PathBuf>,
    },

    /// Return a path to its state as of the commit with uuid specified in `commit`
    Restore {
        #[arg(help = "Paths to restore", required = true)]
        paths: Vec<PathBuf>,

        #[arg(
            short,
            long,
            help = "Keep paths not present in most recent commit; defaults to false",
            default_value_t = false
        )]
        keep: bool,

        #[arg(long, help = "Process paths nonrecursively; defaults to false")]
        flat: bool,

        #[arg(short, long)]
        verbose: bool,
    },

    /// Return the current table to the state it had in the commit with the given uuid `commit`
    Reset {
        #[arg(help = "UUID of the commit to reset to", required = true)]
        commit_uuid: String,

        #[arg(
            short,
            long,
            help = "Keep paths not present in commit; defaults to false",
            default_value_t = false
        )]
        keep: bool,

        #[arg(short, long)]
        verbose: bool,
    },
}
