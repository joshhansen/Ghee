use clap::{CommandFactory, ValueEnum};
use clap_complete::generate_to;
use clap_complete::Shell;
use std::env;
use std::io::Error;

include!("src/lib.rs");

fn main() -> Result<(), Error> {
    let outdir = match env::var_os("OUT_DIR") {
        None => return Ok(()),
        Some(outdir) => outdir,
    };

    let mut cmd = Cli::command();

    for shell in Shell::value_variants() {
        generate_to(
            *shell, &mut cmd, // We need to specify what generator to use
            "mtr",    // We need to specify the bin name manually
            &outdir,  // We need to specify where to write to
        )?;

        //        println!("cargo:warning=completion file is generated: {path:?}");
    }

    Ok(())
}
