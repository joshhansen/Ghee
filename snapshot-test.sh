#!/bin/bash
# Generates example dataset and does some stuff

SCRIPT_PATH=$(realpath $0)

REPO_PATH=$(dirname $SCRIPT_PATH)

OWNER=$(ls -ld $SCRIPT_PATH | cut -d' ' -f3)

cargo build --quiet "$@"

GHEE="$REPO_PATH/target/debug/ghee"

# GHEE="sudo -u $OWNER $GHEE_CMD"

if [ ! -d "snapshot" ]; then
  mkdir snapshot
fi

cd snapshot

EXAMPLE_PATH="$PWD"

set -x

main() {
  # Create a personnel dataset with three people
  # The name, ID, and state of residence are known for each person
  mkdir $1/people
  touch $1/people/Sandeep $1/people/Sofia $1/people/Wulfrum
  $GHEE set -s name=Sandeep -s id=2 -s state=CA $1/people/Sandeep
  $GHEE set -s name=Sofia   -s id=1 -s state=WA $1/people/Sofia
  $GHEE set -s name=Wulfrum -s id=0 -s state=CA $1/people/Wulfrum

  Check how Ghee sees the files we just set up 2> /dev/null
  $GHEE get $1/people

  Tell Ghee what key the data is indexed by 2> /dev/null
  $GHEE init -k name $1/people

  Index the dataset by ID 2> /dev/null
  $GHEE idx -v -k id $1/people $1/people:id

  # Index the dataset by state and ID, placing the index in the default path 2> /dev/null
  $GHEE idx -v -k state -k id $1/people

  # Add more people to the table and its indices 2> /dev/null
  $GHEE ins -v $1/people < $REPO_PATH/people.json

  # Get all of ./people, seen as a table
  $GHEE get -a $1/people

  # # Get all of ./people without sorting in the walk of the folder hierarchy 2> /dev/null
  # ghee get -a --nosort $prefix/people

  # Get the name of everybody from California; include all subpaths (don't ignore indices) 2> /dev/null
  $GHEE get -a -w state=CA -f name $1/people

  # Remove Sofia
  $GHEE del -v $1/people -w name=Sofia
  # Equivalently: ghee del -v ./people Sofia 2> /dev/null

  # Remove Lilly from the :id index and all related indices 2> /dev/null
  $GHEE del -v $1/people:id 3
  # Equivalently: ghee del -v ./people -w id=3
  # Equivalently: ghee del -v ./people Lilly

  # Remove Wulfrum from the :state:id index and all related indices 2> /dev/null
  $GHEE del -v $1/people/:state:id CA 0
  # Equivalently: ghee del -v ./people -w state=CA -w id=0
  # Equivalently: ghee del -v ./people Wulfrum

  # Get the name of everybody from California still remaining,  2> /dev/null
  # taking advantage of the state index                         2> /dev/null
  # This will avoid traversing irrelevant portions of the index 2> /dev/null
  $GHEE get -a -w state=CA -f name $1/people/:state:id

  # Create a table directly 2> /dev/null
  $GHEE create -v $1/direct -k blah

  # Create people2 in one go using `create' 2> /dev/null
  $GHEE create -v $1/people2 -k id < $REPO_PATH/people.json

  $GHEE status $1/people2

  mkdir $1/empty

  $GHEE status $1/empty
  $GHEE get $1/empty
  $GHEE get -a $1/empty
}

btrfs() {
  Sanity check status and log on an empty directory 2> /dev/null
  $GHEE status $1

  $GHEE log

  Create a pizza table 2> /dev/null
  $GHEE create -v -k toppings $1/pizza

  Make sure the status is empty 2> /dev/null
  $GHEE status $1/pizza

  Create a record representing an olive pizza 2> /dev/null
  $GHEE touch $1/pizza/olive

  Check that the new record shows up the status 2> /dev/null
  $GHEE status $1/pizza

  Commit the change with a helpful comment 2> /dev/null
  commit1=$($GHEE commit -m "Add olive pizza" $1/pizza)

  Make sure the new commit shows up in the log 2> /dev/null
  $GHEE log $1/pizza

  Check that the status is empty again, since there are no pending changes 2> /dev/null
  $GHEE status $1/pizza

  Now create a pepperoni pizza 2> /dev/null
  $GHEE touch $1/pizza/pepperoni

  Set text in the file itself, rather than just xattrs 2> /dev/null
  echo "Olives are good on pizza" > $1/pizza/olive

  Also set its yumminess level to 5, as an extended attribute 2> /dev/null
  $GHEE set -s yumminess=5 $1/pizza/olive

  Make sure this all shows up in the status 2> /dev/null
  FIXME The xattr changes arent reflected 2> /dev/null
  $GHEE status $1/pizza

  Commit the new changes 2> /dev/null
  commit2=$($GHEE commit -m "Add pepperoni; add details to olive" $1/pizza)

  Make sure status and log reflect this 2> /dev/null
  $GHEE status $1/pizza

  $GHEE log $1/pizza

  Create a buffalo chicken pizza 2> /dev/null

  $GHEE touch $1/pizza/buffalo-chicken

  pwd
  pushd $1/pizza

  Check status, restore, and status again using the current directory form 2> /dev/null

  $GHEE status

  $GHEE restore .

  $GHEE status

  $GHEE ls

  $GHEE reset $commit1

  $GHEE ls

  popd
  pwd
}

declare -A PREFIXES
PREFIXES=([relative]="." [absolute]="$EXAMPLE_PATH/FSNAME")

mkdir ./ext4
mkdir ./btrfs

## Initial file hierarchy to clone
mkdir ./empty

for i in "${!PREFIXES[@]}"
do
  Make sure we are in the base path so the relative prefix resolves properly 2> /dev/null
  cd $EXAMPLE_PATH

  prefix="${PREFIXES[$i]}"
  "======= $i prefix: $prefix =======" 2> /dev/null
  
  # --- Ext4 ---
  ext4img="ghee-ext4-$i.img"
  dd if=/dev/zero of=$ext4img bs=1M count=400 1> /dev/null 2> /dev/null
  mkfs.ext4 -L ext4-$i $ext4img 1> /dev/null 2> /dev/null
  sudo mount -o loop $ext4img ext4
  sudo chown $OWNER:$OWNER ext4

  fsprefix="${prefix/FSNAME/ext4}"

  cd ext4
  
  main $fsprefix

  cd ..

  sudo umount ext4

  # --- BTRFS ---
  btrfsimg="ghee-btrfs-$i.img"
  dd if=/dev/zero of=$btrfsimg bs=1M count=114 1> /dev/null 2> /dev/null
  mkfs.btrfs -L btrfs-$i --rootdir empty $btrfsimg 1> /dev/null 2> /dev/null
  sudo mount -o loop $btrfsimg btrfs

  sudo chown $OWNER:$OWNER btrfs

  fsprefix="${prefix/FSNAME/btrfs}"

  cd btrfs
  main $fsprefix

  btrfs $fsprefix

  cd ..

  pwd

  sudo umount btrfs
  # --- /BTRFS ---
done

cd ..